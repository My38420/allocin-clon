// fonction pour convertir la durée d'un film récupérée en
// minutes en h et min
const convert = (time) => {
  let reste = time
  let result = ''
  console.log(reste)
  const nbHours = Math.floor(reste / 60)
  reste -= nbHours * 60
  console.log(nbHours)
  console.log(reste)
  if (nbHours > 0) {
    result = result + nbHours + 'h '
  }

  if (reste > 0) {
    result = result + reste + ' min '
  }
  return result
}

const apiKey = '5909c26b75c6101e2b4bf8b3fa86db4e&language=fr'
// on récupère l'ID du film passé quand on a cliqué sur le titre sur la 
// page d'affichage
const idT = localStorage.getItem('idFilm')
// const idR = localStorage.getItem('idReal')
const url = `https://api.themoviedb.org/3/movie/${idT}?api_key=${apiKey}&append_to_response=credits`
// const url = `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${idT}`
console.log(url)
// recherche des infos d'un film sélectionné sur la page de résultats
// via son Id
fetch(url)
  .then((response) => {
    // Récupérer la réponse
    if (response.ok) {
      return response.json()
    } else { // eslint-disable-next-line no-new
      new Error('Erreur réponse HTTP')
    }
  })

  .then((response) => {
    console.log(response)
    // si la promesse précédente a fonctionné
    const film = response
    const urlImg = `https://image.tmdb.org/t/p/w500${film.poster_path}`
    const imgAff = document.createElement('img')
    const title = document.getElementById('title')
    const aff = document.getElementById('aff')
    const resum = document.getElementById('resum')
    const date = document.getElementById('release_date')
    const duree = document.getElementById('duree')
    imgAff.src = urlImg
    imgAff.alt = film.title
    imgAff.classList.add('col-sm-6')
    resum.innerText = film.overview
    title.innerText = film.title
    duree.innerText = convert(film.runtime)
    console.log(film.title)
    console.log(film.runtime)
    aff.appendChild(imgAff)
    const event = new Date(film.release_date)
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }

    const releaseDate = event.toLocaleDateString('fr-FR', options) // DATE DE SORTIE COMPLETE
    // affichage de la date de réalisation du film
    date.innerText = releaseDate
    // Affichage de tous les genres du film
    for (let g = 0; g < film.genres.length; g++) {
      let genre = `gen${g + 1}`
      const idGenre = document.getElementById('genre')
      genre = document.createElement('a')
      genre.id = genre
      genre.innerText = film.genres[g].name
      genre.classList.add('col-sm-2')
      console.log(genre.innerText)
      idGenre.appendChild(genre)
    }
    const crew = film.credits.crew
    // affichage de tous les noms de réalisateur du film
    for (let g = 0; g < crew.length; g++) {
      if (crew[g].job === 'Director') {
        const grpReal = document.getElementById('real')
        console.log(crew[g].job)
        let realis = `real${g + 1}`
        realis = document.createElement('a')
        realis.id = realis
        realis.innerText = crew[g].name
        realis.classList.add('col-sm-2')
        grpReal.appendChild(realis)
      }
    }
    // suppression de la variable locale
    localStorage.removeItem('idFilm')
  })
