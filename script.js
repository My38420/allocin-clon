/* eslint-disable no-unused-expressions */

const film = document.querySelector('#rech')
// clé nécessaire pour accéder aux infos de l'API The Movie DB
const apiKey = '5909c26b75c6101e2b4bf8b3fa86db4e&language=fr'
const pprec = 1
const btn = document.getElementById('envoyer')

// fonction d'affichage d'une page sélectionnée en cliquant sur son numéro
const displayPage2 = (pprec, nbpage, title) => {
  console.log('entree displayPage2')
  console.log(nbpage)

  // on réinitialise la page HTML à vide
  const h4 = document.getElementById('h4')
  h4.innerHTML = ' '

  const zrech = document.getElementById('zrech')
  zrech.innerHTML = ' '

  const div = document.getElementById('liste')
  div.innerHTML = ' '
  const url = `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${title}&page=${nbpage}`
  console.log(url)
  // recherche de la page demandée sur les pages résultats trouvées lors
  // de la recherche initiale des films contenant le texte saisi
  fetch(url)
    .then((response) => {
      // Récupérer la réponse
      if (response.ok) {
        return response.json()
      } else { // eslint-disable-next-line no-new
        new Error('Erreur réponse HTTP')
      }
    })

    .then((response) => {
      // si la promesse précédente a fonctionné
      // eslint-disable-next-line no-console
      const datas = response
      console.log(datas)
      for (var i = 0; i < datas.results.length; i++) {
        const Affichage = document.getElementById('liste')
        const film = datas.results[i]
        console.log(film)
        // s'il n'y a pas d'affiche pour un film, une image de fond est affichée
        // par défaut
        let urlImg = ' '
        if (film.poster_path === null) {
          urlImg = './assets/imgDefault.jpg'
        } else {
          urlImg = `https://image.tmdb.org/t/p/w500${film.poster_path}`
        }
        const row = document.createElement('div')
        const aff = document.createElement('img')
        const title = document.createElement('a')
        const resume = document.createElement('p')
        row.classList.add('row')
        row.classList.add('d-flex')
        // création de la zone d'affichage des films
        Affichage.appendChild(row)
        title.setAttribute('href', 'info.html')
        title.innerText = film.title
        title.classList.add('mt-2', 'col-sm-3')
        resume.classList.add('mt-2', 'col-sm-6')
        title.id = film.id
        aff.src = urlImg
        aff.alt = film.title
        aff.classList.add('mt-2', 'col-sm-3', 'w-25')
        resume.innerText = film.overview
        // ajout des 3 colonnes pour chaque film avec affiche/titre/résumé
        row.appendChild(aff)
        row.appendChild(title)
        row.appendChild(resume)
        const pageId = document.getElementById(`p${nbpage}`)
        pageId.setAttribute('aria-current', 'page')
        pageId.classList.add('active')
        const courId = document.querySelector('a')
        const span = document.createElement('span')
        span.classList.add('sr-only')
        span.innerText = '(current)'
        courId.appendChild(span)
        pprec = nbpage
        console.log(pprec)
      }
      // eslint-disable-next-line no-sequences
    }),
  (error) => {
    // si la promesse précédente a échoué
    // eslint-disable-next-line no-alert
    alert(error)
  }
}
// affichage de la première page de résultats
const displayFilm = (datas) => {
  const nbPages = datas.total_pages
  console.log(datas)
  // on cache la zone de recherche et le titre pour afficher la liste des films
  const h4 = document.getElementById('h4')
  h4.innerHTML = ' '

  const zrech = document.getElementById('zrech')
  zrech.innerHTML = ' '
  if (nbPages > 1) {
    // pour chaque page de résultats, affichage des films
    console.log(nbPages)
    const pageC = Object.values(datas)
    const npage = pageC[0]
    console.log(npage)
    // il y a plus d'une page de résultats, on affiche la pagination
    // const divpag = document.getElementById('divpag')
    for (let p = 1; p <= nbPages & p <= 5; p++) {
      const page = `p${p}`
      const pageId = document.getElementById(page)
      console.log(pageId)
      pageId.style.display = 'block'
      pageId.addEventListener('click', () => { displayPage2(pprec, p, film.value) })
    }
    // eslint-disable-next-line no-undef
    for (let i = 0; i < 20; i++) {
      const Affichage = document.getElementById('liste')
      const film = datas.results[i]
      let urlImg = ' '
      if (film.poster_path === null) {
        urlImg = './assets/imgDefault.jpg'
      } else {
        urlImg = `https://image.tmdb.org/t/p/w500${film.poster_path}`
      }
      const row = document.createElement('div')
      const aff = document.createElement('img')
      const title = document.createElement('a')
      const resume = document.createElement('p')
      row.classList.add('row')
      row.classList.add('d-flex')
      Affichage.appendChild(row)
      title.setAttribute('href', 'info.html')
      title.innerText = film.title
      title.classList.add('mt-2', 'col-sm-2')
      title.id = film.id
      aff.src = urlImg
      aff.setAttribute('width', '20%')
      aff.alt = film.title
      aff.classList.add('mt-2', 'col-sm-3', 'img-thumbnail', 'w-25')
      resume.classList.add('mt-2', 'col-sm-7')
      resume.innerText = film.overview
      row.appendChild(aff)
      row.appendChild(title)
      row.appendChild(resume)
    }
  } else {
    // il n'y a qu'une seule page de résultats
    console.log('une seule page')
    const nbfilm = datas.results.length
    console.log(nbfilm)
    for (let i = 0; i < nbfilm; i++) {
      const Affichage = document.getElementById('liste')
      const film = datas.results[i]
      let urlImg = ' '
      if (film.poster_path === null) {
        urlImg = './assets/imgDefault.jpg'
      } else {
        urlImg = `https://image.tmdb.org/t/p/w500${film.poster_path}`
      }
      const row = document.createElement('div')
      const aff = document.createElement('img')
      const title = document.createElement('a')
      const resume = document.createElement('p')
      row.classList.add('row')
      row.classList.add('d-flex')
      Affichage.appendChild(row)
      title.setAttribute('href', 'info.html')
      title.innerText = film.title
      title.classList.add('mt-2', 'col-sm-2')
      title.id = film.id
      aff.src = urlImg
      aff.alt = film.title
      aff.classList.add('mt-2', 'col-sm-3', 'img-thumbnail', 'w-25')
      resume.classList.add('mt-2', 'col-sm-7')
      resume.innerText = film.overview
      row.appendChild(aff)
      row.appendChild(title)
      row.appendChild(resume)
    }
  }
}

// fonction qui recherche tous les films correspondant au tevte saisi
// dans la zone de recherche
const rechFilm = () => {
  const url = `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${film.value}`
  fetch(url)
    .then((response) => {
      // Récupérer la réponse
      if (response.ok) {
        return response.json()
      } else { // eslint-disable-next-line no-new
        new Error('Erreur réponse HTTP')
      }
    })

    .then((response) => {
      // si la promesse précédente a fonctionné
      // eslint-disable-next-line no-console
      const datas = response
      // appel de la fonction d'affichage pour la 1ère page
      displayFilm(datas)
      // eslint-disable-next-line no-sequences
    }),
  (error) => {
    // si la promesse précédente a échoué
    // eslint-disable-next-line no-alert
    alert(error)
  }
}
// le click sur le bouton chercher appelle la fonction de recherche selon la valeur
// du texte saisi
btn.addEventListener('click', () => { rechFilm() })
// sauvegarde de l'id du film lors d'un click
document.addEventListener('click', (event) => { localStorage.setItem('idFilm', event.target.id) })
